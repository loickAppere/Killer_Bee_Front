import React from 'react';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom';

import { Nav, Alert } from '@/_components';
import { Home } from '@/home';
import { Users } from '@/users';
import { Models } from '@/models';
import { Ingredients } from '@/ingredients';
import { Processes } from '@/processes';
import {Login} from "@/_components/Login";

function App() {
    const { pathname } = useLocation();  

    return (
        <div className="app-container bg-light">
            <Nav />
            <Alert />
            <div className="container pt-4 pb-4">
                <Switch>
                    <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
                    <Route exact path="/" component={Home} />
                    <Route path="/users" component={Users} />
                    <Route path="/models" component={Models} />
                    <Route path="/ingredients" component={Ingredients} />
                    <Route path="/processes" component={Processes}/>
                    <Route path="/authenticate" component={Login}/>
                    <Redirect from="*" to="/" />
                </Switch>
            </div>
        </div>
    );
}

export { App }; 
