import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
    return (
        <div>
            <h1>React - CRUD Example with React Hook Form</h1>
            <p>An example app showing how to list, add, edit and delete user records with React and the React Hook Form library.</p>
            <p><Link to="users">&gt;&gt; Manage Users</Link></p>
            <p><Link to="models">&gt;&gt; Manage Models</Link></p>
            <p><Link to="ingredients">&gt;&gt; Manage Ingredients</Link></p>
            <p><Link to="processes">&gt;&gt; Manage Processes</Link></p>
        </div>
    );
}

export { Home };
