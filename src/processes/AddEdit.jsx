import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import { processService, alertService, modelService } from '@/_services';
import Multiselect from 'multiselect-react-dropdown';
import {fetchWrapper} from "@/_helpers";
import config from 'config';

const res = {options: []};
let models_id =[];

function test(){
    let tmp = modelService.getAll();
    tmp.then(value => {
        for(let i=0; i< value.length ;i++){
            res.options.push(value[i])
        }
    });
}
test();

function onSelect(selectedList, selectedItem) {
    let res=[];
    for(let item in selectedList){
        res.push(selectedList[item].id)
    }
    models_id=res
}

function onRemove(selectedList, removedItem) {
    let res=[];
    for(let item in selectedList){

        if(item != removedItem){
            res.push(selectedList[item].id)
        }
    }
    models_id=res
}

function AddEdit({ history, match }) {
    const { id } = match.params;
    const isAddMode = !id;

    // form validation rules
    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Name is required'),
        steps: Yup.string()
            .required('Add a few steps'),
        description: Yup.string()
            .required('Add the weight of the process')
    });

    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, setValue, errors, formState } = useForm({
        resolver: yupResolver(validationSchema)
    });

    function onSubmit(data) {
        return isAddMode
            ? createProcess(data,models_id)
            : updateProcess(id, data,models_id);
    }

    function createProcess(data, models_id) {
        return processService.create(data, models_id)
            .then(() => {
                alertService.success('Process added', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(alertService.error);
    }

    function updateProcess(id, data, models_id) {
        return processService.update(id, data, models_id)
            .then(() => {
                alertService.success('Process updated', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(alertService.error);
    }

    useEffect(() => {
        if (!isAddMode) {
            // get process and set form fields
            processService.getById(id).then(process => {
                const fields = ['name', 'description', 'ModelId'];
                fields.forEach(field => setValue(field, process[field]));
            });
        }
    }, []);

    return (
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
            <h1>{isAddMode ? 'Add Process' : 'Edit Process'}</h1>
            <div className="form-row">
                <div className="form-group col-5">
                    <label>Name</label>
                    <input name="name" type="text" ref={register} className={`form-control ${errors.firstName ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.name?.message}</div>
                </div>
                <div className="form-group col-5">
                    <label>Steps</label>
                    <input name="steps" type="text" ref={register} className={`form-control ${errors.steps ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.steps?.message}</div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col">
                    <label>Ingredient(s)</label>
                    <Multiselect name="ModelId" ref={register} className={`form-control ${errors.ModelId ? 'is-invalid' : ''}`}
                                 options={res.options} // Options to display in the dropdown
                                 selectedValues={res.selectedValue} // Preselected value to persist in dropdown
                                 onSelect={onSelect} // Function will trigger on select event
                                 onRemove={onRemove} // Function will trigger on remove event
                                 displayValue="name" // Property name to display in the dropdown options
                                 showCheckbox = {true}
                                 singleSelect={true}
                    />
                    <div className="invalid-feedback">{errors.ModelId?.message}</div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-5">
                    <label>Description (add the weight here)</label>
                    <input name="description" type="text" ref={register} className={`form-control ${errors.description ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.description?.message}</div>
                </div>
            </div>
            <div className="form-group">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-primary">
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    Save
                </button>
                <Link to={isAddMode ? '.' : '..'} className="btn btn-link">Cancel</Link>
            </div>
        </form>
    );
}

export { AddEdit };
