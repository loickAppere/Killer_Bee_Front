import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { processService } from '@/_services';

function List({ match }) {
    const { path } = match;
    const [processes, setProcesses] = useState(null);

    useEffect(() => {
        processService.getAll().then(x => setProcesses(x));
    }, []);

    function deleteProcess(id) {
        setProcesses(processes.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        processService.delete(id).then(() => {
            setProcesses(processes => processes.filter(x => x.id !== id));
        });
    }

    return (
        <div>
            <h1>Processes</h1>
                <Link to={`${path}/add`} className="btn btn-sm btn-success mb-2">Add Process</Link>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th style={{ width: '10%' }}>Name</th>
                    <th style={{ width: '60%' }}>Steps</th>
                    <th style={{ width: '30%' }}>Description</th>
                </tr>
                </thead>
                <tbody>
                {processes && processes.map(process =>
                    <tr key={process.id}>
                        <td>{process.name}</td>
                        <td>{process.steps}</td>
                        <td>{process.description}</td>
                        <td style={{ whiteSpace: 'nowrap' }}>
                            <Link to={`${path}/edit/${process.id}`} className="btn btn-sm btn-primary mr-1">Edit</Link>
                            <button onClick={() => deleteProcess(process.id)} className="btn btn-sm btn-danger btn-delete-process" disabled={process.isDeleting}>
                                {process.isDeleting
                                    ? <span className="spinner-border spinner-border-sm"></span>
                                    : <span>Delete</span>
                                }
                            </button>
                        </td>
                    </tr>
                )}
                {!processes &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div>You need to be login</div>
                        </td>
                    </tr>
                }
                {processes && !processes.length &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div className="p-2">No Processes To Display</div>
                        </td>
                    </tr>
                }
                </tbody>
            </table>
        </div>
    );
}

export { List };
