import config from 'config';
import { fetchWrapper } from '@/_helpers';

const baseUrl = `${config.apiUrl}/ingredients`;

export const ingredientService = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

function getAll() {
    let token = localStorage.getItem('token');
    let tmp = {
        token: token,
        "Access-Control-Allow-Origin": "*"
    }
    return fetchWrapper.get(baseUrl, tmp);
}

function getById(id) {
    let token = localStorage.getItem('token');
    let tmp = {
        token: token,
        "Access-Control-Allow-Origin": "*"
    }
    return fetchWrapper.get(`${baseUrl}/${id}`, tmp);
}

function create(params) {
    return fetchWrapper.post(baseUrl, params);
}

function update(id, params) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id) {
    return fetchWrapper.delete(`${baseUrl}/${id}`);
}
