import config from 'config';
import { fetchWrapper } from '@/_helpers';

const baseUrl = `${config.apiUrl}/modelIngredient`;

export const modelIngredientService = {
    getAll,
    getById,
    create,
    update,
    getIngredientByModel,
    delete: _delete
};

function getAll() {
    let token = localStorage.getItem('token');
    return fetchWrapper.get(baseUrl, {headers: token});
}

function getById(id) {
    let token = localStorage.getItem('token');
    return fetchWrapper.get(`${baseUrl}/${id}`, {headers: token});
}

function create(params) {
    return fetchWrapper.post(baseUrl, params);
}

function update(id, params) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params);
}

// prefixed with underscored because delete is a reserved word in javascript
function _delete(id) {
    return fetchWrapper.delete(`${baseUrl}/${id}`);
}
function getIngredientByModel(){
    let token = localStorage.getItem('token');
    return fetchWrapper.get(`${baseUrl}`, {headers: token});
}
