import React from 'react';
import {NavLink, Route, Switch} from 'react-router-dom';
import {userService} from "@/_services";

import {Login} from "@/_components/Login";
import {List} from "@/users/List";

function Nav() {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <div className="navbar-nav">
                <NavLink exact to="/" className="nav-item nav-link">Home</NavLink>
                <NavLink to="/users" className="nav-item nav-link">Users</NavLink>
                <NavLink to="/models" className="nav-item nav-link">Models</NavLink>
                <NavLink to="/ingredients" className="nav-item nav-link">Ingredients</NavLink>
                <NavLink to="/processes" className="nav-item nav-link">Processes</NavLink>
                <NavLink to="/authenticate" className="nav-item nav-link">Login</NavLink>
            </div>
            <div className="user-nav">User : {userService.getCurrent().email}</div>
        </nav>

    );
}

export { Nav };
