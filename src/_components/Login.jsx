import React, { useState } from 'react'
import {fetchWrapper} from "@/_helpers";
import config from 'config';
import regeneratorRuntime from "regenerator-runtime";
import Popup from 'reactjs-popup';


function Login () {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [msg, setMsg] = useState('');
    const Auth = async (e) => {
        e.preventDefault();
        try {
           await fetchWrapper.post(`${config.apiUrl}/authenticate`, {
                email: email,
                password: password
            }).then(response => {
               localStorage.setItem("token",response.accessToken);
               console.log("login success");
           })


        } catch (error) {
            if (error.response) {
                setMsg(error.response.data.msg);
            }
        }
    }

    return (
        <section className="hero has-background-grey-light is-fullheight is-fullwidth">
            <div className="hero-body">
                <div className="container">
                    <div className="columns is-centered">
                        <div className="column is-4-desktop">
                            <form onSubmit={Auth} className="box">
                                <p className="has-text-centered">{msg}</p>
                                <div className="field mt-5">
                                    <label className="label">Email or Username</label>
                                    <div className="controls">
                                        <input type="text" className="input" placeholder="Username" value={email} onChange={(e) => setEmail(e.target.value)} />
                                    </div>
                                </div>
                                <div className="field mt-5">
                                    <label className="label">Password</label>
                                    <div className="controls">
                                        <input type="password" className="input" placeholder="******" value={password} onChange={(e) => setPassword(e.target.value)} />
                                    </div>
                                </div>
                                <div className="field mt-5">
                                    <Popup trigger={ <button className="button is-success is-fullwidth">Login</button>} position="right center">
                                        <div>Login success</div>
                                    </Popup>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export {Login}
