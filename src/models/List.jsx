import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import {ingredientService, modelIngredientService, modelService} from '@/_services';

function List({ match }) {
    const { path } = match;
    let [models, setModels] = useState(null);

    useEffect(() => {
        modelIngredientService.getIngredientByModel().then(x => setModels(x));
    }, []);

    function deleteModel(id) {
        setModels(models.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        modelService.delete(id).then(() => {
            setModels(models => models.filter(x => x.id !== id));
        });
    }

    return (
        <div>
            <h1>Models</h1>
                <Link to={`${path}/add`} className="btn btn-sm btn-success mb-2">Add Model</Link>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th style={{ width: '30%' }}>Name</th>
                    <th style={{ width: '30%' }}>Description</th>
                    <th style={{ width: '20%' }}>Unitary price exclusive of tax</th>
                    <th style={{ width: '10%' }}>Range</th>
                    <th style={{ width: '10%' }}>ingredient</th>
                </tr>
                </thead>
                <tbody>
                {models && models.map(model =>
                    <tr key={model.id}>
                        <td>{model.name}</td>
                        <td>{model.description}</td>
                        <td>{model.upet}</td>
                        <td>{model.range}</td>
                        <td>
                            <div>
                                {model.Ingredients.map( ingredient => (
                                    <p key={ingredient.id}>{ingredient.name} - {ingredient.description}</p>
                                ))}
                            </div>
                            {model.Ingredients && !model.Ingredients.length &&
                                <div>
                                    <div colSpan="4" className="text-center">
                                        <div className="p-2">No ingredients To Display</div>
                                    </div>
                                </div>
                            }
                        </td>
                        <td style={{ whiteSpace: 'nowrap' }}>
                            <Link to={`${path}/edit/${model.id}`} className="btn btn-sm btn-primary mr-1">Edit</Link>
                            <button onClick={() => deleteModel(model.id)} className="btn btn-sm btn-danger btn-delete-model" disabled={model.isDeleting}>
                                {model.isDeleting
                                    ? <span className="spinner-border spinner-border-sm"></span>
                                    : <span>Delete</span>
                                }
                            </button>
                        </td>
                    </tr>
                )}
                {!models &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div>You need to be login</div>
                        </td>
                    </tr>
                }
                {models && !models.length &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div className="p-2">No Models To Display</div>
                        </td>
                    </tr>
                }
                </tbody>
            </table>
        </div>
    );
}

export { List };
