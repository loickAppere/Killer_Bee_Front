import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import { modelService, modelIngredientService, ingredientService,alertService } from '@/_services';
import Multiselect from 'multiselect-react-dropdown';
const res = {options: []};
let ingredients_id =[];

function test(){
    let tmp = ingredientService.getAll();
    tmp.then(value => {
        for(let i=0; i< value.length ;i++){
            res.options.push(value[i])
        }
    });
}
test();

function onSelect(selectedList, selectedItem) {
    let res=[];
    for(let item in selectedList){
        res.push(selectedList[item].id)
    }
    ingredients_id=res
}

function onRemove(selectedList, removedItem) {
    let res=[];
    for(let item in selectedList){

        if(item != removedItem){
            res.push(selectedList[item].id)
        }
    }
    ingredients_id=res
}

function AddEdit({ history, match }) {
    const { id } = match.params;
    const isAddMode = !id;

    // form validation rules
    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Name is required'),
        upet: Yup.number()
            .required('Unitary cost price exclusive of tax is required'),
        range: Yup.string()
            .required('Range is required')
    });

    // functions to build form returned by useForm() hook
    const { register, handleSubmit, reset, setValue, errors, formState } = useForm({
        resolver: yupResolver(validationSchema)
    });

    function onSubmit(data) {
        return isAddMode
            ? createModel(data,ingredients_id)
            : updateModel(id, data,ingredients_id);
    }

    function createModel(data,ingredient_id) {
        return modelService.create(data,ingredient_id)
            .then(() => {
                alertService.success('Model added', { keepAfterRouteChange: true });
                history.push('.');
            })
            .catch(alertService.error);
    }

    function updateModel(id, data, ingredients_id) {
        return modelService.update(id, data, ingredients_id)
            .then(() => {
                alertService.success('Model updated', { keepAfterRouteChange: true });
                history.push('..');
            })
            .catch(alertService.error);
    }

    useEffect(() => {
        if (!isAddMode) {
            // get model and set form fields
            modelService.getById(id).then(model => {
                const fields = ['name', 'description', 'upet', 'range', 'id_ingredient'];
                fields.forEach(field => setValue(field, model[field]));
            });
        }
    }, []);

    return (
        <form onSubmit={handleSubmit(onSubmit)} onReset={reset}>
            <h1>{isAddMode ? 'Add Model' : 'Edit Model'}</h1>
            <div className="form-row">
                <div className="form-group col-5">
                    <label>Name</label>
                    <input name="name" type="text" ref={register} className={`form-control ${errors.firstName ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.name?.message}</div>
                </div>
                <div className="form-group col-5">
                    <label>Description</label>
                    <input name="description" type="text" ref={register} className={`form-control ${errors.description ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.description?.message}</div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col-7">
                    <label>Unitary cost price exclusive of tax</label>
                    <input name="upet" type="text" ref={register} className={`form-control ${errors.upet ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.upet?.message}</div>
                </div>
                <div className="form-group col">
                    <label>Range</label>
                    <input name="range" type="text" ref={register} className={`form-control ${errors.range ? 'is-invalid' : ''}`} />
                    <div className="invalid-feedback">{errors.range?.message}</div>
                </div>
            </div>
            <div className="form-row">
                <div className="form-group col">
                    <label>Ingredient(s)</label>
                    <Multiselect name="id_ingredient" ref={register} className={`form-control ${errors.id_ingredient ? 'is-invalid' : ''}`}
                        options={res.options} // Options to display in the dropdown
                        selectedValues={res.selectedValue} // Preselected value to persist in dropdown
                        onSelect={onSelect} // Function will trigger on select event
                        onRemove={onRemove} // Function will trigger on remove event
                        displayValue="name" // Property name to display in the dropdown options
                        showCheckbox = {true}
                    />
                    <div className="invalid-feedback">{errors.id_ingredient?.message}</div>
                </div>
            </div>
            <div className="form-group">
                <button type="submit" disabled={formState.isSubmitting} className="btn btn-primary">
                    {formState.isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                    Save
                </button>
                <Link to={isAddMode ? '.' : '..'} className="btn btn-link">Cancel</Link>
            </div>
        </form>
    );
}

export { AddEdit };
