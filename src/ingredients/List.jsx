import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { ingredientService } from '@/_services';

function List({ match }) {
    const { path } = match;
    const [ingredients, setIngredients] = useState(null);

    useEffect(() => {
        ingredientService.getAll().then(x => setIngredients(x));
    }, []);

    function deleteIngredient(id) {
        setIngredients(ingredients.map(x => {
            if (x.id === id) { x.isDeleting = true; }
            return x;
        }));
        ingredientService.delete(id).then(() => {
            setIngredients(ingredients => ingredients.filter(x => x.id !== id));
        });
    }

    return (
        <div>
            <h1>Ingredients</h1>
                <Link to={`${path}/add`} className="btn btn-sm btn-success mb-2">Add Ingredient</Link>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th style={{ width: '30%' }}>Name</th>
                    <th style={{ width: '70%' }}>Weight</th>
                </tr>
                </thead>
                <tbody>
                {ingredients && ingredients.map(ingredient =>
                    <tr key={ingredient.id}>
                        <td>{ingredient.name}</td>
                        <td>{ingredient.description}</td>
                        <td style={{ whiteSpace: 'nowrap' }}>
                            <Link to={`${path}/edit/${ingredient.id}`} className="btn btn-sm btn-primary mr-1">Edit</Link>
                            <button onClick={() => deleteIngredient(ingredient.id)} className="btn btn-sm btn-danger btn-delete-ingredient" disabled={ingredient.isDeleting}>
                                {ingredient.isDeleting
                                    ? <span className="spinner-border spinner-border-sm"></span>
                                    : <span>Delete</span>
                                }
                            </button>
                        </td>
                    </tr>
                )}
                {!ingredients &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div>You need to be login</div>
                        </td>
                    </tr>
                }
                {ingredients && !ingredients.length &&
                    <tr>
                        <td colSpan="4" className="text-center">
                            <div className="p-2">No Ingredients To Display</div>
                        </td>
                    </tr>
                }
                </tbody>
            </table>
        </div>
    );
}

export { List };
